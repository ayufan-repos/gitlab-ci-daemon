package tags

type Set map[string]Tags

func (s Set) Equals(other Set) bool {
	if len(s) != len(other) {
		return false
	}

	for index, tags := range s {
		otherTags, ok := other[index]
		if !ok {
			return false
		}

		if !tags.Equals(otherTags) {
			return false
		}
	}
	return true
}

func (s Set) Sort() Set {
	for index := range s {
		s[index].Sort()
	}
	return s
}

func (s Set) IncludesAll(other Set) bool {
	for index, tags := range s {
		otherTags, ok := other[index]
		if !ok {
			continue
		}

		if tags.IncludesAll(otherTags) {
			return true
		}
	}

	return false
}
