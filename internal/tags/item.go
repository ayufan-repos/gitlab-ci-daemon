package tags

type ID interface{}

type Item interface {
	TagsID() ID
	TagsSet() Set
}

type Items map[ID]Item
