package tags

import (
	"sort"
	"strings"
)

type Tags []string

func (t Tags) Equals(other Tags) bool {
	if len(t) != len(other) {
		return false
	}

	for idx, tag := range t {
		if other[idx] != tag {
			return false
		}
	}

	return true
}

func (t Tags) NotEquals(other Tags) bool {
	return !t.Equals(other)
}

func (t Tags) Sort() Tags {
	sort.Strings(t)
	return t
}

func (t Tags) String() string {
	return strings.Join(t, ",")
}

func (t Tags) IncludesAll(other Tags) bool {
	if len(t) < len(other) {
		return false
	}

	idx := 0
	for _, tag := range other {
		found := false

		for idx < len(t) {
			found = (t[idx] == tag)
			idx++
			if found {
				break
			}
		}

		if !found {
			return false
		}
	}

	return true
}
