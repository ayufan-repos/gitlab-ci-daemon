package tags

import "sync"

type tagsMap map[string]*Node

type Tree struct {
	nodes tagsMap
	items Items
	lock  sync.RWMutex
}

func NewTree() *Tree {
	return &Tree{
		nodes: make(tagsMap),
		items: make(Items),
	}
}

func (t *Tree) getNode(key string) *Node {
	return t.nodes[key]
}

func (t *Tree) ensureNode(key string) *Node {
	node := t.nodes[key]
	if node == nil {
		node = NewNode()
		t.nodes[key] = node
	}
	return node
}

func (t *Tree) Get(id ID) Item {
	t.lock.RLock()
	defer t.lock.RUnlock()

	return t.items[id]
}

func (t *Tree) Add(item Item) bool {
	t.lock.Lock()
	defer t.lock.Unlock()

	if _, ok := t.items[item.TagsID()]; ok {
		return false
	}

	t.items[item.TagsID()] = item

	for key, tags := range item.TagsSet() {
		node := t.ensureNode(key)
		node.Add(item, tags)
	}
	return true
}

func (t *Tree) Remove(item Item) bool {
	t.lock.Lock()
	defer t.lock.Unlock()

	if _, ok := t.items[item.TagsID()]; !ok {
		return false
	}

	delete(t.items, item.TagsID())

	for key, tags := range item.TagsSet() {
		node := t.getNode(key)
		if node != nil {
			node.Remove(item, tags)
		}
	}
	return true
}

func (t *Tree) RemoveID(id ID) bool {
	t.lock.RLock()
	item := t.items[id]
	t.lock.RUnlock()
	if item == nil {
		return false
	}

	return t.Remove(item)
}

func (t *Tree) Exists(id ID) bool {
	t.lock.RLock()
	defer t.lock.RUnlock()

	_, ok := t.items[id]
	return ok
}

func (t *Tree) Contains(item Item) bool {
	t.lock.RLock()
	defer t.lock.RUnlock()

	_, ok := t.items[item.TagsID()]
	return ok
}

func (t *Tree) EnumerateAll(enumerator Enumerator) int {
	t.lock.RLock()
	defer t.lock.RUnlock()

	count := 0
	for _, node := range t.nodes {
		count += node.EnumerateAll(enumerator)
	}
	return count
}

func (t *Tree) EnumerateWhenIncludesAll(tagsSet Set, enumerator Enumerator) int {
	t.lock.RLock()
	defer t.lock.RUnlock()

	count := 0

	for key, tags := range tagsSet {
		tags.Sort()
		node := t.getNode(key)
		if node != nil {
			count += node.EnumerateWhenIncludesAll(tags, enumerator)
		}
	}

	return count
}

func (t *Tree) EnumerateWhenSubsetOf(tagsSet Set, enumerator Enumerator) int {
	t.lock.RLock()
	defer t.lock.RUnlock()

	count := 0

	for key, tags := range tagsSet {
		tags.Sort()
		node := t.getNode(key)
		if node != nil {
			count += node.EnumerateWhenSubsetOf(tags, enumerator)
		}
	}

	return count
}

func (t *Tree) Print() {
	t.lock.RLock()
	defer t.lock.RUnlock()

	println("Root:")

	for key, node := range t.nodes {
		println("-", key)
		node.Print(1)
	}
}

func (t *Tree) Truncate() {
	t.lock.Lock()
	defer t.lock.Unlock()

	t.nodes = make(tagsMap)
	t.items = make(Items)
}
