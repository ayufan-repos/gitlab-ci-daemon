package builds

import (
	"encoding/json"
	"strings"

	"github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/client"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/redis"
)

const notificationChannel = "builds:notifications"
const detailsKey = "builds:details:"
const requestBuildLimit int64 = 1000

type notificationsController struct {
	controller *Controller
	newBuilds  chan *Build
}

func (n *notificationsController) requestBuilds(ch chan *client.Build, limit int64) error {
	var afterID int64

	for {
		builds, err := n.controller.client.AllPendingBuilds(afterID, limit)
		if err != nil {
			return err
		}

		for _, build := range builds {
			ch <- build

			if build.Id > afterID {
				afterID = build.Id
			}
		}

		// if we received less than requested,
		// it means that we are done
		if int64(len(builds)) < limit {
			return nil
		}
	}
}

func (n *notificationsController) OnStart() error {
	ch := make(chan *client.Build, requestBuildLimit)
	defer close(ch)

	go func() {
		for build := range ch {
			newBuild := n.controller.AddBuild(build)
			if newBuild != nil {
				n.newBuilds <- newBuild
			}
		}
	}()

	err := n.requestBuilds(ch, requestBuildLimit)
	return errors.Wrap(err, "requestBuilds")
}

func (n *notificationsController) OnEnd() {
	n.controller.InvalidateAll()
}

func (n *notificationsController) OnValue(key, value string) {
	if !strings.HasPrefix(key, detailsKey) {
		logrus.Errorln("Unknown builds notification:", key, "=", value)
		return
	}

	logrus.Debugln("OnValue:", key, "=", value)

	build := client.Build{}
	err := json.Unmarshal([]byte(value), &build)
	if err != nil {
		logrus.Errorln("Failed to deserialize:", key, "=", value, ":", err)
		return
	}

	newBuild := n.controller.AddBuild(&build)
	if newBuild != nil {
		n.newBuilds <- newBuild
	}
}

func (c *Controller) ProcessBuilds(newBuilds chan *Build) {
	redis.WatchChannel(notificationChannel, &notificationsController{
		controller: c,
		newBuilds:  newBuilds,
	})
}
