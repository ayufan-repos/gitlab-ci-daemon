package builds

import "sort"

type BuildList struct {
	builds []*Build
}

func (b *BuildList) Len() int {
	return len(b.builds)
}

func (b *BuildList) Less(i, j int) bool {
	return b.builds[i].Id < b.builds[j].Id
}

func (b *BuildList) Swap(i, j int) {
	tmp := b.builds[i].Id
	b.builds[i].Id = b.builds[j].Id
	b.builds[j].Id = tmp
}

func (b *BuildList) Sort() {
	sort.Sort(b)
}

func (b *BuildList) Pop() *Build {
	if len(b.builds) == 0 {
		return nil
	}

	build := b.builds[0]
	b.builds = b.builds[1:]
	return build
}

func (b *BuildList) search(build *Build) int {
	return sort.Search(len(b.builds), func(i int) bool {
		return b.builds[i].Id >= build.Id
	})
}

func (b *BuildList) Index(build *Build) int {
	idx := b.search(build)
	if idx < len(b.builds) && b.builds[idx].Id == build.Id {
		return idx
	}

	return -1
}

func (b *BuildList) Contains(build *Build) bool {
	return b.Index(build) >= 0
}

func (b *BuildList) Add(build *Build) bool {
	idx := b.search(build)
	if idx < len(b.builds) && b.builds[idx].Id == build.Id {
		return false
	}

	b.builds = append(b.builds[:idx], append([]*Build{build}, b.builds[idx:]...)...)
	return true
}

func (b *BuildList) Remove(build *Build) bool {
	idx := b.Index(build)
	if idx < 0 {
		return false
	}

	b.builds = append(b.builds[:idx], b.builds[idx+1:]...)
	return true
}

func (b *BuildList) Print() {
	for _, build := range b.builds {
		println("-", build.Id)
	}
}
