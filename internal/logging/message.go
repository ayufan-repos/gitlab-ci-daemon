package logging

import (
	"fmt"
	"net/http"
)

func SetMessage(w http.ResponseWriter, reason string, args ...interface{}) {
	rw, ok := w.(ResponseWriter)
	if !ok {
		return
	}

	rw.SetMessage(reason, args...)
}

func Error(w http.ResponseWriter, code int, reason string, args ...interface{}) {
	SetMessage(w, reason, args...)
	http.Error(w, fmt.Sprintf(reason, args...), code)
}
