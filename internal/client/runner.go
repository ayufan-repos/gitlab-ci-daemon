package client

import (
	"errors"
	"net/http"
	"time"

	"github.com/Sirupsen/logrus"

	"gitlab.com/ayufan/gitlab-ci-daemon/internal/tags"
)

type RunnerRequest struct {
	Token string `json:"token"`
}

type Runner struct {
	Id      int64    `json:"id"`
	TagsSet tags.Set `json:"tags_set"`
}

type RunnerRunning struct {
	Id      int64           `json:"id"`
	Running map[int64]int64 `json:"running"`
}

func (g *GitLab) GetRunnerInfo(token string) (runner Runner, err error) {
	started := time.Now()
	result, statusText, _ := g.doJSON("/api/v4/runners/info", "POST", 200, &RunnerRequest{Token: token}, &runner)

	switch result {
	case http.StatusOK:
		// this is expected due to fact that we ask for non-existing job
		logrus.Println("api: runner info...", "received", token, time.Since(started))
		return
	default:
		logrus.WithField("status", statusText).Errorln("api: runner info...", "failed", token, time.Since(started))
		return Runner{}, errors.New(statusText)
	}
}

func (g *GitLab) GetRunnerRunning(token string) (running RunnerRunning, err error) {
	started := time.Now()
	result, statusText, _ := g.doJSON("/api/v4/runners/running", "POST", 200, &RunnerRequest{Token: token}, &running)

	switch result {
	case http.StatusOK:
		// this is expected due to fact that we ask for non-existing job
		logrus.Println("api: runner running builds...", "received", token, time.Since(started))
		return
	default:
		logrus.WithField("status", statusText).Errorln("api: runner running builds...", "failed", token, time.Since(started))
		return RunnerRunning{}, errors.New(statusText)
	}
}
