package profile

import (
	"runtime"
	"time"

	"github.com/Sirupsen/logrus"
)

func PrintMemory() {
	for {
		var m runtime.MemStats
		runtime.ReadMemStats(&m)
		logrus.Printf("[MEM] Alloc=%vkB, TotalAlloc=%vkB, Sys=%vkB, NumGC=%v",
			m.Alloc/1024, m.TotalAlloc/1024, m.Sys/1024, m.NumGC)
		time.Sleep(5 * time.Second)
	}
}
