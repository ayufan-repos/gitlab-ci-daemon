package main

import (
	"flag"
	"net/http"
	"net/url"

	"github.com/Sirupsen/logrus"

	"gitlab.com/ayufan/gitlab-ci-daemon/internal/builds"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/client"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/config"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/profile"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/redis"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/runners"
	"gitlab.com/ayufan/gitlab-ci-daemon/internal/server"
)

var (
	clientURL   = flag.String("client-url", "http://localhost:3000", "the API address of GitLab")
	clientToken = flag.String("client-token", "", "security token to communicate to GitLab")

	redisURL = flag.String("redis-url", "tcp://127.0.0.1:6379", "address of Redis Server")

	listenAddr = flag.String("listen-addr", ":9002", "listen address")

	debug = flag.Bool("debug", false, "Run deamon in debug mode")
)

func getRedisConfig() *config.RedisConfig {
	u, err := url.Parse(*redisURL)
	if err != nil {
		logrus.Fatalln(err)
	}

	return &config.RedisConfig{URL: config.TomlURL{*u}}
}

func main() {
	flag.Parse()

	if *debug {
		logrus.SetLevel(logrus.DebugLevel)
		go profile.PrintMemory()
	}

	buildsCh := make(chan *builds.Build, 1024)

	redis.Configure(getRedisConfig(), redis.DefaultDialFunc)

	logrus.Infoln("Creating GitLab client to", *clientURL, "...")
	gitlab, err := client.NewGitLabClient(*clientURL, *clientToken)
	if err != nil {
		logrus.Fatalln(err)
	}

	logrus.Infoln("Creating builds controller...")
	builds, err := builds.NewController(gitlab)
	if err != nil {
		logrus.Fatalln(err)
	}

	logrus.Infoln("Creating runner controller...")
	runners, err := runners.NewController(gitlab, builds)
	if err != nil {
		logrus.Fatalln(err)
	}

	logrus.Infoln("Creating daemon server...")
	server, err := server.NewDaemonServer(gitlab, runners, builds)
	if err != nil {
		logrus.Fatalln(err)
	}

	logrus.Infoln("Starting runner consume loop...")
	go runners.ConsumeBuilds(buildsCh)

	logrus.Infoln("Starting runner notifications loop...")
	go runners.ProcessNotifications()

	logrus.Infoln("Starting builds processing loop...")
	go builds.ProcessBuilds(buildsCh)

	logrus.Infoln("Listening on", *listenAddr, "...")
	err = http.ListenAndServe(*listenAddr, server)
	if err != nil {
		logrus.Fatalln(err)
	}
}
