# GitLab CI Daemon (PoC)

The purpose of that project is to provide high performance API interface for all CI Runners.
Read more detailed reasoning behind this project: https://gitlab.com/gitlab-org/gitlab-ce/issues/46499#note_92107205.

This project tries to verify how complex is to achieve below goals following the steps outlined.

## Requirements

1. The Daemon is a drop-in replecements for the API server presenting Runner-compatible interface for all Runner communication,
2. Requires Rails with: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/21620,
3. Requires shared Redis with Rails.

## Goals

1. Re-implement most expensive operations using existing API interface for Runners,
1. Make daemon to be drop-in replacement for existing API, hidden behind FF,
1. Be stateful, but keep all logic in single place (GitLab Rails),
1. Mux all Runner requests into a set of new more efficient Rails requests,
1. Implement more efficient interface for Runner once this project gains maturity (GRPC? Websockets? anything else).

Another set of ideas that needs to be considered are commented in: https://gitlab.com/gitlab-org/gitlab-ce/issues/46499#note_92156290

## Steps

1. Make it passthrough proxy for all existing requests (DONE),
1. Support Redis (DONE),
1. Implement `request/job` method and re-implement runner scheduling mechanism (DONE),
1. Use Redis for notifying of Runner change (DONE),
1. Use Redis for notifying of a new build in queue (DONE),
1. Support long pulling of Jobs (DONE),
1. Implement patching trace chunks (DONE, but insecure, no permission checks nor JobStatus),
1. Implement fair scheduling (DONE, but inefficient, as it requests a number of jobs every request),
1. Make it efficent,
1. Make this daemon to sit between Workhorse and Rails,
1. Make a feature flag in Workhorse to pass all Runner specific traffic to Daemon,
1. Automatically cleanup runners after some period of in-use,
1. Refresh Runner activitiy,
1. Figure out how it behaves and write additional steps afterwards.

## How it works

1. Daemon stores a state of all pending builds and recently connected runners,
1. At start it pulls a list of all pending builds from Rails,
1. When a new build is enqueued, Rails sends notification to `builds:notifications` of `builds:details:1={"id":1,"project_id":8,"...}` that is deserialized by daemon,
1. When a runner changes, Rails sends notification to `runners:notifications` of `runner:details:SHARED_RUNNER_TOKEN=79db4be0f165f968e4fd25bf9c6ab56c` to invalidate Runner on Daemon side,
1. When a new Runner connects, daemon requests information about Runner via `/runners/info` endpoint,
1. Daemon looks at a list of pending builds and creates per-runner queue,
1. Daemon sends to Rails request to assign build to given runner,
1. Rails perform final permission checks on given build and uses resource locking,
1. If job is not present, runner is watching on queue for given interval.

## Gods and bads / problems and notes

1. It performs `O(n)` matching of builds for runner, and runners for build (NO LONGER TRUE),
1. It performs indexed-tags builds search for given runner, it implements something similar to Trie to perform efficient tree traversal to quickly match any number of tags, this is guided by indexes (to group by project, group or shared) provided as part of tag set,
1. It extends existing (shadows) API calls, rather than implementing them end-to-end via separate API: take a look at `PatchTrace` it is not maintanble in that form, even though it works,
1. It does not use any data store, rather in-memory array/map for holding all Runners and Builds, likely better approach would be NoSQL or some in-memory database,
1. It fetches all pending builds at start, even though it is very efficient and non-blocking: fetched builds are assigned once being fetched it still takes around (1. 120ms per-1k if using Rails-SQL-method (feature: `ci_pending_jobs_raw_sql`), 2. 600ms per-1k if using Rails-AR-models-method),
1. It offers quite general matcher: params (provided by Runner) and conditions (verified by Build), but is that OK to model all features (currently it is),
1. It is horizontally scalable, because our brain is still Rails,
1. It holds a snippet of all builds in memory (only conditions),
1. 100k builds requires 100MB of working memory,
1. Runner queue of 10k builds requires around 600kB of memory,
1. Likely we should have separate set of APIs on Rails to interface with daemon as a first-class rather than shadowing existing one and rework current Runner API along way when we add a first-class API,
1. Actually it is very performant once the `O(n)` is fixed, it can easily process 100k builds, and support any number of runners, and assign builds dynamically.

## API

### Runner information

This is the payload that describes Runner parameters:

```json
$ curl -X POST -F token=f93aab5a3ab3db917e29d1ae55a0c3 http://localhost:3000/api/v4/runners/info | jq
{
  "id": 1,
  "tags_set": {
    "shared": [
      "run_untagged",
      "tag_docker"
    ]
  }
}
```

### Pending builds information

This is the payload that describes all pending jobs:

```json
$ curl -X POST -F token=f93aab5a3ab3db917e29d1ae55a0c3 http://localhost:3000/api/v4/runners/info | jq
[
  {
    "id": 118518,
    "project_id": 8,
    "tags_set": {
      "project_8": [
        "protected",
        "tag_docker",
        "tag_secure",
      ],
      "group_10": [
        "protected",
        "tag_docker",
        "tag_secure",
      ],
      "shared": [
        "protected",
        "tag_docker",
        "tag_secure",
      ]
    }
  },
  {
    "id": 118519,
    "project_id": 8,
    "tags_set": {
      "project_8": [
        "run_untagged"
      ],
      "group_7": [
        "run_untagged"
      ],
      "shared": [
        "run_untagged"
      ]
    }
  }
]
```

### Asign job to runner

This replicates `/jobs/request` schema, but works on specific build and runner:

```json
$ curl -X POST -F token=f93aab5a3ab3db917e29d1ae55a0c3 'http://localhost:9002/api/v4/jobs/request/111 | jq
{
  "allow_git_fetch": "true",
  "artifacts": null,
  "cache": [
    null
  ],
  "credentials": [],
  "dependencies": [],
  "features": {
    "trace_sections": "true"
  },
  "git_info": {
    "before_sha": "0000000000000000000000000000000000000000",
    "ref": "master",
    "ref_type": "branch",
    "repo_url": "http://gitlab-ci-token:iyYFcAFUriBoCC9gfVis@localhost:8181/documentcloud/underscore.git",
    "sha": "0763ab3985c0cbf7ba1b719f11d2c08e35e42c1d"
  },
  "id": 111,
  "image": null,
  "job_info": {
    "name": "rspec:windows 2 3",
    "project_id": 37,
    "project_name": "Underscore",
    "stage": "test"
  },
  "runner_info": {
    "runner_session_url": null,
    "timeout": 3600
  },
  "services": [],
  "steps": [
    {
      "allow_failure": "false",
      "name": "script",
      "script": [
        "bash"
      ],
      "timeout": 3600,
      "when": "on_success"
    }
  ],
  "token": "iyYFcAFUriBoCC9gfVis",
  "variables": [
    {
      "key": "CI_PIPELINE_ID",
      "public": "true",
      "value": "11"
    },
    ...
  ]
}
```

### Information about jobs per-project for given runner

This payload presents amount of jobs running per-project.
This likely will be used to load-balance jobs between different projects.

```json
$ curl -X POST -F token=f93aab5a3ab3db917e29d1ae55a0c3 http://localhost:3000/api/v4/runners/running | jq
[
  {
    "project_id": 38,
    "count": 26
  },
  {
    "project_id": 36,
    "count": 22
  },
  {
    "project_id": 37,
    "count": 21
  }
]
```

## Notifications

### Build notification

A notification to `builds:notifications` is sent when a new build is enqueued.

It contains json payload, the same content as for pending builds:

```json
builds:details:1={"id":1,"project_id":8,"tags_set":[...]}
```

### Runner notification

A notification to `runners:notifications` is sent when a runner configuration changes (tags, project, group assignement).

It contains a new random key describing a state of the configuration:

```
runner:details:SHARED_RUNNER_TOKEN=79db4be0f165f968e4fd25bf9c6ab56c
```

When received, daemon invalidates all runner assignements and makes it re-request data from Rails once runner connects again.
